﻿namespace MoqExamples.Domain;

using Microsoft.AspNetCore.Http;

public class LoginService
{
    private readonly ILoginClient loginClient;

    public LoginService(ILoginClient loginClient)
    {
        this.loginClient = loginClient;
    }

    public async Task LoginUser(string email, string password, HttpContext context)
    {
        // ...please don't ever do real auth this way. This example is a little contrived for the sake of the example

        if (context.Request.Cookies.ContainsKey("AuthToken"))
        {
            return;
        }

        var credentials = new UserCredentials
        {
            Email = email,
            Password = password
        };

        var token = await loginClient.Login(credentials);
        context.Items["AuthToken"] = token.Token;
    }
}
