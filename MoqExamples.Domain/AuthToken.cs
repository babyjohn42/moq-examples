﻿namespace MoqExamples.Domain;

public class AuthToken
{
    public string Token { get; set; }
}
