﻿namespace MoqExamples.Domain;

using System.Net.Http.Json;
using System.Text.Json;

public class LoginClient : ILoginClient
{
    private readonly HttpClient httpClient;
    private readonly Uri Path = new("https://reqres.in/api/login");

    public LoginClient(HttpClient httpClient)
    {
        this.httpClient = httpClient;
    }

    public async Task<AuthToken> Login(UserCredentials credentials)
    {
        using var body = new StringContent(JsonSerializer.Serialize(credentials));
        using var response = await httpClient.PostAsync(Path, body);
        response.EnsureSuccessStatusCode();
        return await response.Content.ReadFromJsonAsync<AuthToken>();
    }
}
