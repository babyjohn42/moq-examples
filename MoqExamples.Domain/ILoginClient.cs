﻿namespace MoqExamples.Domain;

public interface ILoginClient
{
    Task<AuthToken> Login(UserCredentials credentials);
}