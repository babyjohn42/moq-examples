﻿namespace MoqExamples.UnitTests.HttpClients.Helpers;

public static class HttpRequestMessageExtensions
{
    /// <summary>
    /// Safely read the entire HTTP request body content as a string, returning null if empty.
    /// </summary>
    public static Task<string> ReadRequestBody(this HttpRequestMessage request)
    {
        return request?.Content?.ReadAsStringAsync() ?? Task.FromResult<string>(null);
    }
}
