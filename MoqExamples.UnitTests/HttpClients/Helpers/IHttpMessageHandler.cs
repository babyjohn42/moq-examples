﻿namespace MoqExamples.UnitTests.HttpClients.Helpers;

/// <summary>
/// A mock HTTP message handler, simulating the method signature of real HTTP message handlers.
/// </summary>
public interface IHttpMessageHandler
{
    Task<HttpResponseMessage> SendAsync(HttpRequestMessage request, CancellationToken cancellationToken);
}
