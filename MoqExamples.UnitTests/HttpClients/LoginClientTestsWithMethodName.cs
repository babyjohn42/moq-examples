﻿namespace MoqExamples.UnitTests.HttpClients;

using KellermanSoftware.CompareNetObjects;
using Moq;
using Moq.Protected;
using MoqExamples.Domain;
using MoqExamples.UnitTests.HttpClients.Helpers;
using NUnit.Framework;
using System.Net;
using System.Text.Json;

[TestFixture]
public class LoginClientTestsWithMethodName
{
    private LoginClient loginClient;

    private Mock<HttpMessageHandler> mockHttpMessageHandler;

    [SetUp]
    public void SetUp()
    {
        // HttpClient uses an inner HttpMessageHandler to actually do all of its requests, and that object is much easier to mock.
        mockHttpMessageHandler = new Mock<HttpMessageHandler>();
        var httpClient = new HttpClient(mockHttpMessageHandler.Object);

        loginClient = new LoginClient(httpClient);
    }

    [Test]
    public async Task Login_SuccessfulResponse_ReturnsExpected()
    {
        // Arrange
        var request = new UserCredentials
        {
            Email = "yep@test.com",
            Password = "pa$$word"
        };

        var expectedResponse = new AuthToken
        {
            Token = "open_sesame"
        };

        var expectedRequestUri = new Uri("https://reqres.in/api/login");

        using var response = new HttpResponseMessage
        {
            StatusCode = HttpStatusCode.OK,
            Content = new StringContent(JsonSerializer.Serialize(expectedResponse))
        };

        HttpRequestMessage actualRequest = default;
        string actualRequestBody = default;

        mockHttpMessageHandler
            .Protected()
            .Setup<Task<HttpResponseMessage>>("SendAsync", ItExpr.IsAny<HttpRequestMessage>(), ItExpr.IsAny<CancellationToken>()) // all HTTP client methods end up calling this one
            .Callback<HttpRequestMessage, CancellationToken>(async (request, _) =>
            {
                // if you care to capture the actual request and/or its body, this is how you'd do it.
                // if you want the body you have to capture it now because of stream disposal timing.
                actualRequest = request;
                actualRequestBody = await request.ReadRequestBody(); // helper extension method
            })
            .ReturnsAsync(response);

        // Act
        var actualResponse = await loginClient.Login(request);

        // Assert
        actualResponse.ShouldCompare(expectedResponse);
        Assert.AreEqual(HttpMethod.Post, actualRequest.Method);
        actualRequest.RequestUri.ShouldCompare(expectedRequestUri);
        Assert.AreEqual(JsonSerializer.Serialize(request), actualRequestBody);

        mockHttpMessageHandler
            .Protected()
            .Verify("SendAsync", Times.Once(), actualRequest, ItExpr.IsAny<CancellationToken>());
    }

    [Test]
    public void Login_UnsuccessfulResponse_Throws()
    {
        // Arrange
        var request = new UserCredentials
        {
            Email = "yep@test.com",
            Password = "pa$$word"
        };

        var expectedRequestUri = new Uri("https://reqres.in/api/login");

        using var response = new HttpResponseMessage
        {
            StatusCode = HttpStatusCode.InternalServerError
        };

        HttpRequestMessage actualRequest = default;
        string actualRequestBody = default;

        mockHttpMessageHandler
            .Protected()
            .Setup<Task<HttpResponseMessage>>("SendAsync", ItExpr.IsAny<HttpRequestMessage>(), ItExpr.IsAny<CancellationToken>())
            .Callback<HttpRequestMessage, CancellationToken>(async (request, _) =>
            {
                actualRequest = request;
                actualRequestBody = await request.ReadRequestBody(); // helper extension method
            })
            .ReturnsAsync(response);

        // Act/Assert
        var actualException = Assert.ThrowsAsync<HttpRequestException>(() => loginClient.Login(request));

        Assert.AreEqual(HttpStatusCode.InternalServerError, actualException.StatusCode);
        Assert.AreEqual(HttpMethod.Post, actualRequest.Method);
        actualRequest.RequestUri.ShouldCompare(expectedRequestUri);
        Assert.AreEqual(JsonSerializer.Serialize(request), actualRequestBody);

        mockHttpMessageHandler
            .Protected()
            .Verify("SendAsync", Times.Once(), actualRequest, ItExpr.IsAny<CancellationToken>());
    }
}
