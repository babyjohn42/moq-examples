﻿namespace MoqExamples.UnitTests.LinqToMocks;

using Microsoft.AspNetCore.Http;
using Moq;
using MoqExamples.Domain;
using NUnit.Framework;

[TestFixture]
public class LoginServiceTestsWithoutLinq
{
    private LoginService loginService;

    private Mock<ILoginClient> mockLoginClient;

    [SetUp]
    public void SetUp()
    {
        mockLoginClient = new Mock<ILoginClient>();

        loginService = new LoginService(mockLoginClient.Object);
    }

    [Test]
    public async Task Login_NoCookie_AddsToContextItems()
    {
        // Arrange
        var email = "foo@bar.com";
        var password = "abc123";
        var token = new AuthToken { Token = "open_sesame" };
        var contextItems = new Dictionary<object, object>();

        mockLoginClient
            .Setup(mock => mock.Login(It.Is<UserCredentials>(c => c.Email == email && c.Password == password)))
            .ReturnsAsync(token)
            .Verifiable();

        var mockCookies = new Mock<IRequestCookieCollection>();
        var mockRequest = new Mock<HttpRequest>();
        var mockHttpContext = new Mock<HttpContext>();

        // ...this gets old quick.
        mockHttpContext.SetupGet(mock => mock.Items).Returns(contextItems).Verifiable();
        mockHttpContext.SetupGet(mock => mock.Request).Returns(mockRequest.Object).Verifiable();
        mockRequest.SetupGet(mock => mock.Cookies).Returns(mockCookies.Object).Verifiable();
        mockCookies.Setup(mock => mock.ContainsKey("AuthToken")).Returns(false).Verifiable();

        // Act
        await loginService.LoginUser(email, password, mockHttpContext.Object);

        // Assert
        Assert.AreEqual("open_sesame", contextItems["AuthToken"]);
        mockLoginClient.Verify();
        mockHttpContext.Verify();
        mockRequest.Verify();
        mockCookies.Verify();
    }

    [Test]
    public async Task Login_HasCookie_Returns()
    {
        // Arrange
        var email = "foo@bar.com";
        var password = "abc123";

        var mockCookies = new Mock<IRequestCookieCollection>();
        var mockRequest = new Mock<HttpRequest>();
        var mockHttpContext = new Mock<HttpContext>();

        // ...this gets old quick.
        mockHttpContext.SetupGet(mock => mock.Request).Returns(mockRequest.Object).Verifiable();
        mockRequest.SetupGet(mock => mock.Cookies).Returns(mockCookies.Object).Verifiable();
        mockCookies.Setup(mock => mock.ContainsKey("AuthToken")).Returns(true).Verifiable();
        // Act
        await loginService.LoginUser(email, password, mockHttpContext.Object);

        // Assert
        mockLoginClient.VerifyNoOtherCalls();
        mockHttpContext.Verify();
        mockRequest.Verify();
        mockCookies.Verify();
    }
}
