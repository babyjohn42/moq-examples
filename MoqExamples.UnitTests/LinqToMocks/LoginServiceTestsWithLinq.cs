﻿namespace MoqExamples.UnitTests.LinqToMocks;

using Microsoft.AspNetCore.Http;
using Moq;
using MoqExamples.Domain;
using NUnit.Framework;

[TestFixture]
public class LoginServiceTestsWithLinq
{
    private LoginService loginService;

    private Mock<ILoginClient> mockLoginClient;

    [SetUp]
    public void SetUp()
    {
        mockLoginClient = new Mock<ILoginClient>();

        loginService = new LoginService(mockLoginClient.Object);
    }

    [Test]
    public async Task Login_NoCookie_AddsToContextItems()
    {
        // Arrange
        var email = "foo@bar.com";
        var password = "abc123";
        var token = new AuthToken { Token = "open_sesame" };
        var contextItems = new Dictionary<object, object>();

        mockLoginClient
            .Setup(mock => mock.Login(It.Is<UserCredentials>(c => c.Email == email && c.Password == password)))
            .ReturnsAsync(token)
            .Verifiable();            

        // you can create quick mocks with multiple setups using linq syntax, including nested setups
        var mockHttpContext = Mock.Of<HttpContext>(
            mock => mock.Items == contextItems
            && mock.Request.Cookies.ContainsKey("AuthToken") == false);

        // Act
        await loginService.LoginUser(email, password, mockHttpContext);

        // Assert
        Assert.AreEqual("open_sesame", contextItems["AuthToken"]);
        mockLoginClient.Verify();

        // you can then get the underlying mock from the mocked object later if you need it
        Mock.Get(mockHttpContext).VerifyGet(mock => mock.Items, Times.Once);

        // even nested ones you didn't explicitly create!
        Mock.Get(mockHttpContext.Request.Cookies).Verify(mock => mock.ContainsKey("AuthToken"), Times.Once);
    }

    [Test]
    public async Task Login_HasCookie_Returns()
    {
        // Arrange
        var email = "foo@bar.com";
        var password = "abc123";

        var mockHttpContext = Mock.Of<HttpContext>(mock => mock.Request.Cookies.ContainsKey("AuthToken") == true);

        // Act
        await loginService.LoginUser(email, password, mockHttpContext);

        // Assert
        mockLoginClient.VerifyNoOtherCalls();
        Mock.Get(mockHttpContext.Request.Cookies).Verify(mock => mock.ContainsKey("AuthToken"), Times.Once);
    }
}
